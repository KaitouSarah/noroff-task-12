﻿using System;
using System.Collections.Generic;

namespace Noroff_Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a size (PS: 2 and 3 does not have a solution): ");
            int size = Int32.Parse(Console.ReadLine());
            Console.Write($"Enter a x-coordinate for your queen between 1 and {size}: ");
            int x = Int32.Parse(Console.ReadLine()) - 1;
            Console.Write($"Enter a y-coordinate for your queen between : ");
            int y = Int32.Parse(Console.ReadLine()) - 1;

            solveNQ(size, x, y);
        }

        static int solutionCounter = 1;

        static void printSolution(int[,] board, int size)
        {
            Console.Write("{0}-\n", solutionCounter++);
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                    Console.Write(" {0} ", board[i, j]);
                Console.Write("\n");
            }
            Console.Write("\n");
        }

        static bool isSafe(int[,] board, int row, int column, int size)
        {
            int i, j;

            for (i = 0; i < column; i++)
                if (board[row, i] == 1)
                    return false;

            for (i = row, j = column; i >= 0 && j >= 0; i--, j--)
                if (board[i, j] == 1)
                    return false;


            for (i = row, j = column; j >= 0 && i < size; i++, j--)
                if (board[i, j] == 1)
                    return false;

            return true;
        }

        static bool solveNQUtil(int[,] board, int column, int size, int x, int y)
        {
            if (column == size)
            {
                if (board[y, x] == 1)
                {
                    Console.WriteLine("The coordinates for your queen gave this possible solution: ");
                    printSolution(board, size);
                    return true;
                }
            }

            bool res = false;
            for (int i = 0; i < size; i++)
            {
                if (isSafe(board, i, column, size))
                {
                    board[i, column] = 1;

                    res = solveNQUtil(board, column + 1, size, x, y) || res;

                    board[i, column] = 0; 
                }
            }
            return res;
        }

        static void solveNQ(int size, int x, int y)
        {
            int[,] board = new int[size, size];

            if (solveNQUtil(board, 0, size, x, y) == false)
            {
                Console.Write("The coordinates you gave for your queen has no solutions.");
                return;
            }

            return;
        }
    }
}

