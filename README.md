Collaboration with Shangavi.

Task 12: Group Task - Part 2
• Write a program that takes in a co-ordinate on the board
• It must then tell me if it is possible to complete the 8-queens problem with a queen at the chosen location
• If it is possible it must print a board (in console) that shows one such possible solution

Note 1: Forgot to branch early.
Note 2: Sometimes wrote func instead of feat in commits.
Note 3: Got irritated at my first solution, starteed a new project to try something else, and kinda forgot to commit anything until done-ish
Note 4: There is a branch with a different solution, made after the deadline because I wanted to figure out the empy array-bug (and I did)